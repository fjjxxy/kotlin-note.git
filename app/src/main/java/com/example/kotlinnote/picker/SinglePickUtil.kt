package com.example.kotlinnote.picker

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import com.example.kotlinnote.R
import java.util.*

class SinglePickUtil(context: Context?) : Dialog(context!!, R.style.DialogTheme) {
    interface OnSelectedListener {
        fun onSelected(selectedValue: Any?)
    }

    interface OnSelectedPositionListener {
        fun onSelected(selectedValue: Any?, position: Int)
    }

    private var mOnSelectedListener: OnSelectedListener? = null
    private var mOnSelectedPositionListener: OnSelectedPositionListener? = null
    private var mWheelPicker: WheelPicker? = null
    private var mTvSure: TextView? = null
    private var mTvTitle: TextView? = null
    private var mTvCancel: TextView? = null
    private var mTitle: CharSequence? = null
    private var mData: List<*>? = null
    private var mCurrentSelectedData: Any? = null
    private var mPosition = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pop_picker)
        //        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//去掉动画,防止连续弹出两次对话框会出现闪屏现象
        val window = window
        val layoutParams = window!!.attributes
        layoutParams.gravity = Gravity.BOTTOM
        //        layoutParams.y = BarUtils.isNavBarVisible(getWindow()) ? BarUtils.getStatusBarHeight() : 0;
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT
        layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
        window.attributes = layoutParams
        mTvSure = findViewById(R.id.tv_sure)
        mTvCancel = findViewById(R.id.tv_cancel)
        mTvTitle = findViewById(R.id.tv_title)
        mWheelPicker = findViewById(R.id.wheelpicker)
        resetData(mData)
        setTitle(mTitle)
        initListener()
    }

    override fun setTitle(title: CharSequence?) {
        super.setTitle(title)
        mTitle = title
        if (mTvTitle != null) mTvTitle!!.text = title
    }

    fun selectItemByPosition(position: Int) {
        if (mWheelPicker != null) {
            mWheelPicker!!.selectedItemPosition = position
        }
        if (mData != null && !mData!!.isEmpty()) {
            mCurrentSelectedData = mData!![position]
            mPosition = position
        }
    }

    fun resetData(data: List<*>?) {
        mData = data
        if (mWheelPicker != null) mWheelPicker?.data = mData
        if (mData != null && !mData!!.isEmpty()) {
            mCurrentSelectedData = mData!![0]
            mPosition = 0
        }
    }

    fun resetData(data: Array<String?>) {
        mData = Arrays.asList(*data)
        if (mWheelPicker != null) mWheelPicker?.data = mData
        if (mData != null && !(mData as MutableList<*>).isEmpty()) {
            mCurrentSelectedData = (mData as MutableList<*>).get(0)
            mPosition = 0
        }
    }

    fun setOnSelectedListener(onSelectedListener: OnSelectedListener?) {
        mOnSelectedListener = onSelectedListener
    }

    fun setOnSelectedPositionListener(onSelectedPositionListener: OnSelectedPositionListener?) {
        mOnSelectedPositionListener = onSelectedPositionListener
    }

    private fun initListener() {
        mTvSure!!.setOnClickListener {
            if (mOnSelectedListener != null) {
                mOnSelectedListener!!.onSelected(mCurrentSelectedData)
            }
            if (mOnSelectedPositionListener != null) {
                mOnSelectedPositionListener!!.onSelected(mCurrentSelectedData, mPosition)
            }
            dismiss()
        }
        mTvCancel!!.setOnClickListener { dismiss() }
        mWheelPicker!!.setOnItemSelectedListener(object : IWheelPicker.OnItemSelectedListener {
            override fun onItemSelected(picker: WheelPicker?, data: Any?, position: Int) {
                mCurrentSelectedData = data
                mPosition = position
            }

        })
    }

    fun setSelectedItem(position: Int) {
        mWheelPicker!!.selectedItemPosition = position
        if (mData != null && !mData!!.isEmpty()) {
            mCurrentSelectedData = mData!![position]
            mPosition = position
        }
    }

    fun setSelectItem(position: Int) {
        if (mData == null) {
            return
        }
        if (mData!!.size < position) {
            mPosition = 0
            return
        }
        mPosition = position
        mCurrentSelectedData = mData!![position]
        if (mWheelPicker != null) {
            mWheelPicker!!.data = mData
            mWheelPicker!!.selectedItemPosition = mPosition
        }
    }
}
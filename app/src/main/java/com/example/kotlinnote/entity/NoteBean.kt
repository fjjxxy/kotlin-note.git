package com.example.kotlinnote.entity

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

class NoteBean : Serializable {
    var note_id: String? = null
    var title: String? = null
    var content: String? = null
    var add_time: String? = null
    var update_time: String? = null
    var note_code //用来查询备忘录,时间戳
            : String? = null
}
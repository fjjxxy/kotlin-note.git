package com.example.kotlinnote.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlinnote.R
import com.example.kotlinnote.entity.NoteBean
import com.example.kotlinnote.ui.db.SQLiteDB
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_write_note.*
import java.text.SimpleDateFormat
import java.util.*

class WriteNoteActivity : AppCompatActivity(), OnFocusChangeListener {

    private var mType = 0
    private var mIsShowOk = false //是否正在显示完成的按钮--说明这个时候在编辑
    private var mSQLiteDB: SQLiteDB? = null
    private var mTitle: String? = null
    private var mContent: String? = null
    private var mId: String? = null
    private var mNoteCode: String? = null
    private val mList: List<String> = ArrayList()
    private val mStringList: List<String> = ArrayList()
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_note)
        mTitle = intent.getStringExtra(EXTRA_TITLE)
        mContent = intent.getStringExtra(EXTRA_CONTENT)
        mId = intent.getStringExtra(EXTRA_ID)
        mSQLiteDB = SQLiteDB.Companion.getInstance(this)
        mType = intent.getIntExtra(EXTRA_TYPE, mType)
        edit_content!!.onFocusChangeListener = this
        edit_title!!.onFocusChangeListener = this
        if (mType == EXTRA_ADD) {
            tv_title!!.text = "新增"
            iv_ok!!.visibility = View.VISIBLE
            mIsShowOk = true
        } else if (mType == EXTRA_UPDATE) {
            tv_title!!.text = "编辑"
            edit_title!!.setText(mTitle)
            edit_content!!.setText(mContent)
            iv_ok!!.visibility = View.GONE
            mIsShowOk = false
        }
        iv_ok!!.setOnClickListener { addNote() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        when (v.id) {
            R.id.edit_title, R.id.edit_content -> if (!mIsShowOk && hasFocus) {
                iv_ok!!.setOnClickListener { updateNote() }
                iv_ok!!.visibility = View.VISIBLE
                mIsShowOk = true
            }
        }
    }

    /**
     * 新增
     */
    private fun addNote() {
        val title = edit_title!!.text.toString()
        val content = edit_content!!.text.toString()
        val date = Date()
        val addTime = SimpleDateFormat("yyyy-MM-dd HH:mm").format(date)
        val updateTime = SimpleDateFormat("yyyy-MM-dd HH:mm").format(date)
        mNoteCode = date.time.toString() + ""
        val noteBean = NoteBean()
        noteBean.title = title
        noteBean.content = content
        noteBean.add_time = addTime
        noteBean.update_time = updateTime
        noteBean.note_code = mNoteCode
        if (mSQLiteDB!!.addNote(noteBean)) {
            Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show()
            mIsShowOk = false
            iv_ok!!.visibility = View.GONE
            edit_content!!.clearFocus()
            edit_title!!.clearFocus()
        } else {
            Toast.makeText(this, "保存失败", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * 修改
     */
    private fun updateNote() {
        var dataByCode: NoteBean? = null
        if (mType == EXTRA_ADD) {
            dataByCode = mSQLiteDB!!.findDataByCode(mNoteCode)
        }
        val title = edit_title!!.text.toString()
        val content = edit_content!!.text.toString()
        val date = Date()
        val updateTime = SimpleDateFormat("yyyy-MM-dd HH:mm").format(date)
        mNoteCode = date.time.toString() + ""
        val noteBean = NoteBean()
        noteBean.note_id =
            if (mType == EXTRA_ADD) dataByCode?.note_id else mId
        noteBean.title = title
        noteBean.content = content
        noteBean.update_time = updateTime
        if (mSQLiteDB!!.updateNote(noteBean)) {
            Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show()
            mIsShowOk = false
            iv_ok!!.visibility = View.GONE
            edit_content!!.clearFocus()
            edit_title!!.clearFocus()
        } else {
            Toast.makeText(this, "保存失败", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        const val EXTRA_ADD = 1
        const val EXTRA_UPDATE = 2
        const val EXTRA_TYPE = "EXTRA_TYPE" //是新增还是修改
        const val EXTRA_TITLE = "EXTRA_TITLE" //标题
        const val EXTRA_CONTENT = "EXTRA_CONTENT" //内容
        const val EXTRA_ID = "EXTRA_ID" //备忘录id
    }
}
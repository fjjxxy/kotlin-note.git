package com.example.kotlinnote.ui

import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.kotlinnote.entity.NoteBean
import com.example.kotlinnote.R

class NoteGridAdapter(data: MutableList<NoteBean>?) :
    BaseQuickAdapter<NoteBean, BaseViewHolder>(R.layout.item_note_grid, data) {
    private var tvNoteTitle: TextView? = null
    private var tvNoteTitle2: TextView? = null
    private var tvNoteLastTime: TextView? = null
    private var tvNoteContent: TextView? = null
    override fun convert(helper: BaseViewHolder, item: NoteBean) {
        tvNoteTitle = helper.getView<View>(R.id.tv_note_title) as TextView
        tvNoteLastTime = helper.getView<View>(R.id.tv_note_last_time) as TextView
        tvNoteContent = helper.getView<View>(R.id.tv_note_content) as TextView
        tvNoteTitle2 = helper.getView(R.id.tv_note_title2)
        tvNoteTitle2!!.text = item.title
        tvNoteTitle!!.text = item.title
        tvNoteContent!!.text = item.content
        if (!TextUtils.isEmpty(item.update_time)) {
            val times = item.update_time!!.split(" ".toRegex()).toTypedArray()
            tvNoteLastTime!!.text = times[0]
        }
    }
}
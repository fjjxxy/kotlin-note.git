package com.example.kotlinnote.ui.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.database.sqlite.SQLiteOpenHelper

class DataBaseHelp(context: Context?, name: String?, factory: CursorFactory?, version: Int) :
    SQLiteOpenHelper(context, name, factory, version) {
    override fun onCreate(db: SQLiteDatabase) {
        val sql = "create table note(note_id INTEGER PRIMARY KEY AUTOINCREMENT ,title varchar(20)" +
                ",content TEXT,add_time DATETIME,update_time DATETIME,note_code TEXT)"
        db.execSQL(sql)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}
    override fun onOpen(db: SQLiteDatabase) {
        super.onOpen(db)
    }

    companion object {
        private const val DATABASE_NAME = "note.db" //数据库名字
        private const val DATABASE_VERSION = 1 //数据库版本号
        private val mSQLiteDB: SQLiteDB? = null
    }
}
package com.example.kotlinnote.ui

import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.kotlinnote.entity.NoteBean
import com.example.kotlinnote.R
import java.text.SimpleDateFormat
import java.util.*

class NoteAdapter(data: MutableList<NoteBean>?) :
    BaseQuickAdapter<NoteBean, BaseViewHolder>(R.layout.item_note, data) {
    private var tvNoteTitle: TextView? = null
    private var tvNoteLastTime: TextView? = null
    private var tvNoteContent: TextView? = null
    private var tvDelete: TextView? = null
    override fun convert(helper: BaseViewHolder, item: NoteBean) {
        tvNoteTitle = helper.getView<View>(R.id.tv_note_title) as TextView
        tvNoteLastTime = helper.getView<View>(R.id.tv_note_last_time) as TextView
        tvNoteContent = helper.getView<View>(R.id.tv_note_content) as TextView
        tvNoteTitle!!.text = item.title
        tvNoteContent!!.text = item.content
        tvDelete = helper.getView(R.id.tv_delete)
        if (!TextUtils.isEmpty(item.update_time)) {
            val times = item.update_time!!.split(" ".toRegex())!!.toTypedArray()
            val date = Date()
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            if (simpleDateFormat.format(date) == times[0]) {
                tvNoteLastTime!!.text = times[1]
            } else {
                tvNoteLastTime!!.text = item.update_time
            }
        }
    }
}
package com.example.kotlinnote.ui.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.kotlinnote.entity.NoteBean
import java.util.*

class SQLiteDB(context: Context?) {
    fun addNote(noteBean: NoteBean): Boolean {
        var flag = false
        val contentValues = ContentValues()
        contentValues.put("title", noteBean.title)
        contentValues.put("content", noteBean.content)
        contentValues.put("add_time", noteBean.add_time)
        contentValues.put("update_time", noteBean.update_time)
        contentValues.put("note_code", noteBean.note_code)
        val insert = mSQLiteDatabase.insert("note", null, contentValues)
        if (insert >= 1) {
            flag = true
        }
        return flag
    }

    fun updateNote(noteBean: NoteBean): Boolean {
        var flag = false
        val contentValues = ContentValues()
        contentValues.put("title", noteBean.title)
        contentValues.put("content", noteBean.content)
        contentValues.put("update_time", noteBean.update_time)
        contentValues.put("note_code", noteBean.note_code)
        val update =
            mSQLiteDatabase.update("note", contentValues, "note_id=?", arrayOf(noteBean.note_id))
        if (update >= 1) {
            flag = true
        }
        return flag
    }

    fun deleteNote(id: String?): Boolean {
        var flag = false
        val delete = mSQLiteDatabase.delete("note", "note_id=?", arrayOf(id))
        if (delete >= 1) {
            flag = true
        }
        return flag
    }

    fun findAllData(orderBy: String): List<NoteBean> {
        val noteBeans: MutableList<NoteBean> = ArrayList()
        val cursor = mSQLiteDatabase.query(
            "note",
            arrayOf("note_id", "title", "content", "add_time", "update_time", "note_code"),
            null,
            null,
            null,
            null,
            orderBy + if ("title" == orderBy) " asc" else " desc"
        )
        if (cursor != null) {
            while (cursor.moveToNext()) {
                val noteBean = NoteBean()
                noteBean.note_id = cursor.getString(cursor.getColumnIndex("note_id"))
                noteBean.title = cursor.getString(cursor.getColumnIndex("title"))
                noteBean.content = cursor.getString(cursor.getColumnIndex("content"))
                noteBean.add_time = cursor.getString(cursor.getColumnIndex("add_time"))
                noteBean.update_time = cursor.getString(cursor.getColumnIndex("update_time"))
                noteBean.note_code = cursor.getString(cursor.getColumnIndex("note_code"))
                noteBeans.add(noteBean)
            }
        }
        return noteBeans
    }

    fun findDataByContent(content: String, orderBy: String): List<NoteBean> {
        val noteBeans: MutableList<NoteBean> = ArrayList()
        val cursor = mSQLiteDatabase.query(
            "note",
            arrayOf("note_id", "title", "content", "add_time", "update_time", "note_code"),
            "content like '%$content%' or title like '%$content%'",
            null,
            null,
            null,
            orderBy + if ("title" == orderBy) " asc" else " desc"
        )
        if (cursor != null) {
            while (cursor.moveToNext()) {
                val noteBean = NoteBean()
                noteBean.note_id = cursor.getString(cursor.getColumnIndex("note_id"))
                noteBean.title = cursor.getString(cursor.getColumnIndex("title"))
                noteBean.content = cursor.getString(cursor.getColumnIndex("content"))
                noteBean.add_time = cursor.getString(cursor.getColumnIndex("add_time"))
                noteBean.update_time = cursor.getString(cursor.getColumnIndex("update_time"))
                noteBean.note_code = cursor.getString(cursor.getColumnIndex("note_code"))
                noteBeans.add(noteBean)
            }
        }
        return noteBeans
    }

    fun findDataByCode(code: String?): NoteBean {
        val noteBean = NoteBean()
        val cursor =
            mSQLiteDatabase.rawQuery("select *  from note where note_code=?", arrayOf(code))
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                noteBean.note_id = cursor.getString(cursor.getColumnIndex("note_id"))
                noteBean.title = cursor.getString(cursor.getColumnIndex("title"))
                noteBean.content = cursor.getString(cursor.getColumnIndex("content"))
                noteBean.add_time = cursor.getString(cursor.getColumnIndex("add_time"))
                noteBean.update_time = cursor.getString(cursor.getColumnIndex("update_time"))
                noteBean.note_code = cursor.getString(cursor.getColumnIndex("note_code"))
            }
        }
        return noteBean
    }

    companion object {
        private const val DATABASE_NAME = "note.db" //数据库名字
        private const val DATABASE_VERSION = 1 //数据库版本号
        private var mSQLiteDB: SQLiteDB? = null
        private lateinit var mSQLiteDatabase: SQLiteDatabase
        fun getInstance(context: Context?): SQLiteDB? {
            if (mSQLiteDB == null) {
                mSQLiteDB = SQLiteDB(context)
            }
            return mSQLiteDB
        }
    }

    init {
        val dataBaseHelp = DataBaseHelp(context, DATABASE_NAME, null, DATABASE_VERSION)
        mSQLiteDatabase = dataBaseHelp.readableDatabase
    }
}